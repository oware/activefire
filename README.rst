WFP Active Fires Test
========================
  
 WFP Active Fires Test

=========
Exercise 1 
=========

The shell script (scripts/upload.sh) uses the shp2pgsl commandline utility
to upload the downloaded country and hotspots shapefile to their respective tables
in the database.


=========
Exercise 2 
=========

The django project (activefire) has one application (map) where the geodjango Country and Hotspot models are defined.
Django Tastypie framework is used to define an API through the views grab geojson data
to overlay on the map. 

