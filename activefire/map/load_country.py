import os
from django.contrib.gis.utils import LayerMapping
from models import Country

country_mapping = {
    'iso2' : 'ISO2',
    'iso3' : 'ISO3',
    'name' : 'NAME',
    'pop2005' : 'POP2005',
    'geom' : 'MULTIPOLYGON',
}

world_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), '/home/tux/tmp/wfp/', 'TM_WORLD_BORDERS-0.3.shp'))

def run(verbose=True):
    lm = LayerMapping(Country, world_shp, country_mapping,
                      transform=False, encoding='iso-8859-1')

    lm.save(strict=True, verbose=verbose)
