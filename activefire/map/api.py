from tastypie.contrib.gis.resources import ModelResource
from activefire.map.models import Country, Hotspot

class CountryResource(ModelResource):
	class Meta:
		resource_name = 'countries'
		queryset = Country.objects.all()


class HotspotResource(ModelResource):
	class Meta:
		resource_name = 'hotspots'
		queryset = Hotspot.objects.all()

