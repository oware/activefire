from django.contrib.gis.db import models

class Country(models.Model):
    # fields
    name = models.CharField(max_length=50)
    pop2005 = models.IntegerField('Population 2005')
    iso2 = models.CharField('2 Digit ISO', max_length=2)
    iso3 = models.CharField('3 Digit ISO', max_length=3)

    # geometry field
    geom = models.MultiPolygonField()

    objects = models.GeoManager()

    # Returns the string representation of the model.
    def __str__(self):              
        return self.name



class Hotspot(models.Model):
    # fields
    scan = models.FloatField()
    track = models.FloatField()
    acq_date = models.CharField('Acq Date', max_length=20)
    acq_time = models.CharField('Acq Time', max_length=20)
    satellite = models.CharField('Satellite', max_length=20)
    
    
	# geometry field
    geom = models.PointField()

    objects = models.GeoManager()

    # Returns the string representation of the model.
    def __str__(self):             
        return self.acq_time
