import os
from django.contrib.gis.utils import LayerMapping
from models import Hotspot

hotspot_mapping = {
    'scan' : 'SCAN',
    'track' : 'TRACK',
    'acq_date' : 'ACQ_DATE',
    'acq_time' : 'ACQ_TIME',
    'satellite' : 'SATELLITE',
    'geom' : 'POINT',
}

hotspot_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), '/home/tux/tmp/wfp/', 'Global_24h.shp'))

def run(verbose=True):
    lm = LayerMapping(Hotspot, hotspot_shp, hotspot_mapping,
                      transform=False, encoding='iso-8859-1')

    lm.save(strict=True, verbose=verbose)
