function applyMargins() {
        var leftToggler = $(".mini-submenu-left");
        var rightToggler = $(".mini-submenu-right");
        if (leftToggler.is(":visible")) {
          $("#map .ol-zoom")
            .css("margin-left", 0)
            .removeClass("zoom-top-opened-sidebar")
            .addClass("zoom-top-collapsed");
        } else {
          $("#map .ol-zoom")
            .css("margin-left", $(".sidebar-left").width())
            .removeClass("zoom-top-opened-sidebar")
            .removeClass("zoom-top-collapsed");
        }
        if (rightToggler.is(":visible")) {
          $("#map .ol-rotate")
            .css("margin-right", 0)
            .removeClass("zoom-top-opened-sidebar")
            .addClass("zoom-top-collapsed");
        } else {
          $("#map .ol-rotate")
            .css("margin-right", $(".sidebar-right").width())
            .removeClass("zoom-top-opened-sidebar")
            .removeClass("zoom-top-collapsed");
        }
      }

      function isConstrained() {
        return $("div.mid").width() == $(window).width();
      }

      function applyInitialUIState() {
        if (isConstrained()) {
          $(".sidebar-left .sidebar-body").fadeOut('slide');
          $(".sidebar-right .sidebar-body").fadeOut('slide');
          $('.mini-submenu-left').fadeIn();
          $('.mini-submenu-right').fadeIn();
        }
      }

      $(function(){
        $('.sidebar-left .slide-submenu').on('click',function() {
          var thisEl = $(this);
          thisEl.closest('.sidebar-body').fadeOut('slide',function(){
            $('.mini-submenu-left').fadeIn();
            applyMargins();
          });
        });

        $('.mini-submenu-left').on('click',function() {
          var thisEl = $(this);
          $('.sidebar-left .sidebar-body').toggle('slide');
          thisEl.hide();
          applyMargins();
        });

        $('.sidebar-right .slide-submenu').on('click',function() {
          var thisEl = $(this);
          thisEl.closest('.sidebar-body').fadeOut('slide',function(){
            $('.mini-submenu-right').fadeIn();
            applyMargins();
          });
        });

        $('.mini-submenu-right').on('click',function() {
          var thisEl = $(this);
          $('.sidebar-right .sidebar-body').toggle('slide');
          thisEl.hide();
          applyMargins();
        });

        $(window).on("resize", applyMargins);

        /*
        var map = new ol.Map({
          target: "map",
          layers: [
            
            new ol.layer.Tile({
              source: new ol.source.OSM({crossOrigin: null})
            })
            

          ],
          view: new ol.View({
            center: [36.6894427, -1.3298634],
            zoom: 5
          })
        }); */
        
        var map = new L.Map('map');

        // create osm tile layer 
        var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib='RCMRD &copy; OpenStreetMap';
        var osm = new L.TileLayer(osmUrl, {minZoom: 5, maxZoom: 12, attribution: osmAttrib});   

        var mapquest =  L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/{type}/{z}/{x}/{y}.{ext}', {
          type: 'map',
          ext: 'jpg',
          attribution: 'Tiles Courtesy of <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          subdomains: '1234'
        });

        // set centre
        map.setView(new L.LatLng(0.35, 37.63),3);
        map.addLayer(mapquest);

var _url = 'http://activefire.lumtemaps.com/countries/countries/?format=json&limit=0';

$.ajax({
                                type: "GET",
                                url: _url,
                                async: false,
                                dataType: "jsonp",
                                crossDomain: true,
                                //dataType: 'jsonp',
                                success: function(data){
                                    
                                    //alert(data.objects.length);
                                    countries = L.geoJson(data.objects).addTo(map);

                                }

                            });

        
        applyInitialUIState();
        applyMargins();
      });
