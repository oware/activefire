
        
        var map = new L.Map('map');

        // create osm tile layer 
        var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib='RCMRD &copy; OpenStreetMap';
        var osm = new L.TileLayer(osmUrl, {minZoom: 5, maxZoom: 12, attribution: osmAttrib});   

        var mapquest =  L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/{type}/{z}/{x}/{y}.{ext}', {
          type: 'map',
          ext: 'jpg',
          attribution: 'Tiles Courtesy of <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          subdomains: '1234'
        });

        // set centre
        map.setView(new L.LatLng(0.35, 37.63),6);
        map.addLayer(mapquest);
        
       