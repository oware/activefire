from django.conf.urls import patterns, include, url
from activefire.map.api import CountryResource, HotspotResource

from django.contrib import admin
admin.autodiscover()

country_resource = CountryResource()
hotspot_resource = HotspotResource()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'activefire.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	
	# home view
	url(r'^$', 'activefire.views.home', name='home'),

	# hotspot detail view
	url(r'^detail/', 'activefire.views.hotspot', name='hotspot'),
	
	url(r'countries/', include(country_resource.urls)),
	url(r'hotspots/', include(hotspot_resource.urls)),

    url(r'^admin/', include(admin.site.urls)),
)
